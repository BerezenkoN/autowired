package com.anahoret.annotations.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * create by User on 23.01.17.
 */

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})

public @interface Component {
}
