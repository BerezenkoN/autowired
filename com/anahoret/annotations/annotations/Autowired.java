package com.anahoret.annotations.annotations;

import java.lang.annotation.*;

@Target(ElementType.FIELD, ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface Autowired {
}
