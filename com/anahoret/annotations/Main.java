package com.anahoret.annotations;

import com.anahoret.annotations.annotations.MyAnnotation;
import com.anahoret.annotations.container.Container;
import com.anahoret.annotations.modules.ModuleB;

import java.lang.annotation.Annotation;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        ModuleB moduleB = new ModuleB();
        System.out.println("Without container");
        System.out.println(moduleB);

        System.out.println("-----------");

        System.out.println("With container");
        ModuleB bean = (ModuleB) Container.getBean(ModuleB.class);
        System.out.println(bean);

        System.out.println("-----------");

        Annotation[] parentDeclaredAnnot = Parent.class.getDeclaredAnnotations();
        Annotation[] parentAnnot = Parent.class.getAnnotations();

        Annotation[] childDeclaredAnnot = Child.class.getDeclaredAnnotations();
        Annotation[] childAnnot = Child.class.getAnnotations();

        System.out.println(Arrays.toString(parentDeclaredAnnot));
        System.out.println(Arrays.toString(parentAnnot));
        System.out.println(Arrays.toString(childDeclaredAnnot));
        System.out.println(Arrays.toString(childAnnot));

        System.out.println(Child.class.getDeclaredAnnotation(MyAnnotation.class));
        System.out.println(Child.class.getAnnotation(MyAnnotation.class));

        MyAnnotation myAnnotation = Child.class.getAnnotation(MyAnnotation.class);
        System.out.println(myAnnotation.value());

    }

    @MyAnnotation(value = "Hello")
    private class Parent {
    }

    private class Child extends Parent {
    }

}
