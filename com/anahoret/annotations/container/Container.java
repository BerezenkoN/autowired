package com.anahoret.annotations.container;

import com.anahoret.annotations.annotations.Autowired;
import com.anahoret.annotations.annotations.Component;
import com.anahoret.annotations.annotations.Singleton;

import java.awt.*;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.String;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;
import java.util.HashSet;

public class Container {
    private static final Map<Class<?>, Object> singletons = new HashMap<>();

    public static Object getBean(Class<?> clazz) throws Exception {
Object singleton = singletons.get(clazz);
        if(singleton != null) return singleton;
        try{
            boolean isComponent = clazz.getDeclaredAnnotation(Component.class) != null;
            if(!isComponent)
                throw new IllegalArgumentException("Class" + clazz.getCanonicalName() + "is not a component");
            {
                Constructor con = clazz.getConstructor();
                Object instance = constructor.newInstance();
                Set<String> calledSetters = new HashSet<>();

                Method[] methods = clazz.getDeclaredMethods();
                Field[] fields = clazz.getDeclaredFields();

                for (Method method : methods) {
                    if (method.getName().startWith("set") && method.getDeclaredAnnotation(Autowired.class) != null) {
                        Object setterParam = getBean(method.getParameterTypes()[0]);
                        method.invoke(instance, setterParam);
                        calledSetters.add(decapitalize(method.getName().replaceAll("set.", "")));
                    }
                }


                for (Field field : fields) {
                    Autowired autowired = field.getAnnotation(Autowired.class);
                    if (autowired != null && !calledSetters.contains(field.getName())) {
                        Object fieldValue = getBean(field.getType());
                        boolean oldAccesible = field.isAccessible();
                        field.setAccessible(true);
                        field.set(instance, fieldValue);
                        field.setAccessible(oldAccesible);
                    }
                }
                if (clazz.getDeclaredAnnotation(Singleton.class) != null)
                    singletons.put(clazz, instance);


            return instance;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


